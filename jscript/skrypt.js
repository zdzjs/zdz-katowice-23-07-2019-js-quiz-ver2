document.querySelector('nav').
		querySelector('.button').
		addEventListener('click', zdarzenie => {
			var przycisk= 
				zdarzenie.target.tagName.toLowerCase()==='p' ? 
				zdarzenie.target.parentNode : 
				zdarzenie.target;
			//console.log(przycisk, zdarzenie);
			
			if (!przycisk.hide) {
				/*przycisk.parentNode.style.gridTemplateColumns=`auto ${przycisk.clientWidth}px`;
				przycisk.parentNode.style.width=przycisk.clientWidth+"px";
				setTimeout(() => {
					przycisk.parentNode.style.gridTemplateColumns="0 100%";
					przycisk.children[0].innerText=">";
				}, 1000);*/
				przycisk.parentNode.style.transform="translate3d(-90%,0,0)";
				//przycisk.children[0].innerText=">";
				przycisk.children[0].style.transform="rotate3d(0,0,1,-180deg)";
			}
			else {
				//przycisk.parentNode.style.gridTemplateColumns=`auto ${przycisk.clientWidth}px`;
				przycisk.parentNode.style="";
				przycisk.children[0].style="";
				//przycisk.children[0].innerText="<";
				

			}
			przycisk.hide=!przycisk.hide;
		});

function generujPytania() {
	//ponieramy element naszego menu pytań, w którym będą umieszczone
	var nawigacja = document.querySelector("#poleOdpowiedzi");
	//czyścimy całą zawartość menu, która ewentualnie jeszcze się w nim znajduje
	while(nawigacja.children.length) 
		nawigacja.removeChild(nawigacja.children[nawigacja.children.length-1]);
	nawigacja.pozostaloOdpowiedzi=pytaniaOdpowiedzi.length;
	//przeskakujemy po wszystkich dodanych pytaniach
	for(let i=0;i<pytaniaOdpowiedzi.length;i++) {
		//tworzymy dowiązania/uchwyt do elementów w dokumencie
		let elem = document.createElement('div');
		let opis = document.createElement('p');
		//podajemy nazwę pytania, pod jaką będzie ono widoczne
		//w naszym menu
		opis.innerText = pytaniaOdpowiedzi[i].nazwa;
		//prawidłową odpiwiedź dokładamy do zmiennej JavaScript elementu HTML
		//dzięki temu uczestnik nie będzie jej widział w kodzie HTML 
		//UWAGA! W fazie produkcji w ogóle ta zmienna nie powinna być 
		//ustawiana a jedynie referencja do pytania z odpowiedzią po stronie 
		//serwera (obecnie nie używamy)
		elem.odp=pytaniaOdpowiedzi[i]['prawidlowa'];
		//dodajmey pola z pytaniem oraz odpowiedziami; te elementy mogą
		//być widoczne w HTML i na stronie
		elem.dataset.pytanie = pytaniaOdpowiedzi[i]['pytanie'];
		elem.dataset.odpa = pytaniaOdpowiedzi[i]['a'];
		elem.dataset.odpb = pytaniaOdpowiedzi[i]['b'];
		elem.dataset.odpc = pytaniaOdpowiedzi[i].c;
		elem.dataset.odpd = pytaniaOdpowiedzi[i].d;
		//do tak utworzonego elementu dodajemy nasłuch zdarzenia onclick
		//dzięki któremu będziemy ładować nasze pytanie do elementu właściwego
		//quizu
		elem.addEventListener('click', zdarzenie => {
			//tworzymy zmienną uchwyt (pomocniczą) dzięki której będziemy
			//mieć pewność, że pracujemy z poprawnym elementem HTML (zawierającym
			//nasze pytanie i odpowiedzi - pola dataset)
			let elem = zdarzenie.target.tagName.toLowerCase()==='p' ? zdarzenie.target.parentNode : zdarzenie.target;
			//pobieramy element z testem
			let sekcja = document.querySelector("#pytanie");
			//wrzucamy pytatanie
			sekcja.querySelector('h3').innerText = elem.dataset.pytanie;
			sekcja.pytanieNav = elem;
			sekcja.querySelectorAll('span').forEach(function(pobranySpan) {	
				//sekcja.pytanieNav.dataset.dane=pobranySpan.parentNode.children[0].value;
				pobranySpan.parentNode.children[0].checked=false;
				if (sekcja.pytanieNav.dataset.udzielonaOdp) {
					if(sekcja.pytanieNav.dataset.udzielonaOdp===pobranySpan.parentNode.children[0].value)
						pobranySpan.parentNode.children[0].checked=true;
				}
				pobranySpan.innerText=elem.dataset['odp'+pobranySpan.parentNode.children[0].value];
				pobranySpan.parentNode.children[0].addEventListener('click', zdarzenie => {
					if (!sekcja.pytanieNav.dataset.udzielonaOdp)
						nawigacja.pozostaloOdpowiedzi--;
					sekcja.pytanieNav.dataset.udzielonaOdp=zdarzenie.target.value;
					if (nawigacja.pozostaloOdpowiedzi===0)
						document.querySelector('#dzialanie button').disabled=false;
				});
			});
		});
		elem.appendChild(opis);
		nawigacja.appendChild(elem);
	}
}

document.querySelector('#dzialanie button').addEventListener('click', e => {
	var odpowiedziArticle = document.querySelector('#odpowiedzi div div');
	odpowiedziArticle.innerHTML="";
	document.querySelectorAll("#poleOdpowiedzi div").forEach( pole => {
		odpowiedziArticle.innerHTML+=`<div><h4>Odpowiedź na pytanie: 
			"${pole.dataset.pytanie}"</h4>
			<p class='${pole.odp===pole.dataset.udzielonaOdp ? `poprawna'>jest poprawna i` : 
			`bledna'>jest błędna. Odpowiedź poprawna`} brzmi: 
			"${pole.dataset['odp'+pole.odp]}"</p>`;
		//poniższy kod zastąpiony powyższym
		/*if(pole.odp===pole.dataset.udzielonaOdp) {
			odpowiedziArticle.innerHTML+='<div><h4>Odpowiedź na pytanie: "' +
			pole.dataset.pytanie + '"</h4><p>jest poprawna i brzmi: "' + pole.dataset['odp'+pole.odp] +
			'"</p>';
		}
		else {
			odpowiedziArticle.innerHTML+='<div><h4>Odpowiedź na pytanie: "' +
			pole.dataset.pytanie + '"</h4><p>jest błędna. Odpowiedź poprawna brzmi: "' + pole.dataset['odp'+pole.odp] +
			'"</p>';
		}*/
	}); 
	odpowiedziArticle.parentNode.parentNode.style.display="block";
	setTimeout(() => {
	odpowiedziArticle.parentNode.parentNode.style="display:block;top:0;left:0;width:100%;height:100%;";
	}, 500);
});	

function odpowiedziClick(zdarzenie, obiekt) {
	if(!obiekt) return;
	
	obiekt.style="display: block;";
	setTimeout(() => {
		obiekt.style="";
	}, 2000);
	zdarzenie.stopPropagation();
}

{
	let odpowiedziArticle = document.querySelector('#odpowiedzi div');
	odpowiedziArticle.addEventListener('click', e => {
		odpowiedziClick(e, odpowiedziArticle.parentNode);
	},true);
	
	Array.from(odpowiedziArticle.children).forEach( dziecko => {
		dziecko.addEventListener('click', e => {
			odpowiedziClick(e)
		},true);
	});
}

var przerwanieCzas = setInterval(() => {
							if (pytaniaOdpowiedzi) {
								if (przerwanieCzas) 
									clearInterval(przerwanieCzas);
								generujPytania();
							}
						},200);